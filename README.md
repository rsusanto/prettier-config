# `@rsusanto/prettier-config`

Prettier shareable configuration for my personal projects.

## Installation

```bash
npm install --save-dev prettier @prettier/plugin-php @rsusanto/prettier-config
```

## Usage

**Edit `package.json`**:

```jsonc
{
  // ...
  "prettier": "@rsusanto/prettier-config"
}
```
